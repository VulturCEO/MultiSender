## Multi Sender Token ERC20

Allows to any holder of erc20 tokens to make multiple transfers with variables amounts.

### How to use

1. Approve the use of yours tokens: Go to the Smartcontract ERC20 token and use the approve function with the MultiSender contract address
2. Go to MultiSend function and use the Token Contract Address
3. Paste the Address token in array: ["0x0", "0x1", 0x2"]
4. Paste the Amount like this: [100,200,3000] Remember, add the token decimals.

