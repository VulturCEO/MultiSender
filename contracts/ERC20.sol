pragma solidity ^0.4.24;

contract ERC20 {

    function transfer(address _to, uint256 _value) public returns(bool);

    function transferFrom(address from, address to, uint value) public returns(bool);

    function balanceOf(address tokenOwner) public view returns(uint balance);
}