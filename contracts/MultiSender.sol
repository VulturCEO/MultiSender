pragma solidity ^ 0.4 .24;

import "./ERC20.sol";

import "./Ownable.sol";

/**
* @title MultiSender ERC20 token SmartContract 
*/

contract MultiSender is Ownable {

    address public tokenAddr;
    ERC20 public token;


/**
* @dev Allows to "transferFrom" any ERC20 token with multi address and amount of tokens
* _tokenAddr: Token Contract Address
* dest: Array of wallet to send the token
* tokens: Amount each dest of tokens
* Be carefull with the token decimals
* Remember, the "transferFrom" require approval
*/
    function multiSendAnyToken(address _tokenAddr, address[] dests, uint[] tokens) public returns(bool) {
        token = ERC20(_tokenAddr);
        uint256 i = 0;
        while (i < dests.length) {
            require(token.transferFrom(msg.sender, dests[i], tokens[i]));
            i += 1;
        }
        return true;
    }


    /**
     * @dev Function to get the locked tokens back, in case of any issue
    */
    function withdrawRemainingTokens() public onlyOwner {
        uint contractTokenBalance = token.balanceOf(this);
        require(contractTokenBalance > 0);
        token.transfer(owner, contractTokenBalance);
    }

    /**
     * @dev Method to get any ERC20 tokens in the contract
    */
    function withdrawERC20ToOwner(address _erc20) public onlyOwner {
        ERC20 erc20Token = ERC20(_erc20);
        uint contractTokenBalance = erc20Token.balanceOf(this);
        require(contractTokenBalance > 0);
        erc20Token.transfer(owner, contractTokenBalance);
    }

}